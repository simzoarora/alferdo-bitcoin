const usersData = [
  { Sno:'1', username: 'Jason Stataham', status: 'Pending', confidenceLevel: 70, paymentMethod: 'Bitcoin', limit: '5-2000', price: '$15,000', buy: 'Buy'},
  { Sno:'2', username: 'Paul Walker', status: 'Pending', confidenceLevel: 50, paymentMethod: 'Bitcoin', limit: '5-2000', price: '$12,000', buy: 'Buy'},
  { Sno:'3', username: 'Van Diesel', status: 'Pending', confidenceLevel: 70, paymentMethod: 'Bitcoin', limit: '5-2000', price: '$15,000', buy: 'Buy'},
  { Sno:'4', username: 'Anthony M. Iles', status: 'Pending', confidenceLevel: 30, paymentMethod: 'Bitcoin', limit: '5-2000', price: '$15,000', buy: 'Buy'},
  { Sno:'5', username: 'Lawrence V. Davis', status: 'Pending', confidenceLevel: 50, paymentMethod: 'Bitcoin', limit: '5-2000', price: '$15,000', buy: 'Buy'},
]

export default usersData


