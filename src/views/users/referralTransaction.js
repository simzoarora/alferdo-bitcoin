const referralTransaction = [
  { Sno: '1', username: 'Jason Stataham', confidenceLevel: 70, referralTransaction: 20, earning: '$15,000', },
  { Sno: '2', username: 'Paul Walker', confidenceLevel: 50, referralTransaction: 20, earning: '$12,000', },
  { Sno: '3', username: 'Van Diesel', confidenceLevel: 70, referralTransaction: 20, earning: '$15,000', },
  { Sno: '4', username: 'Anthony M. Iles', confidenceLevel: 30, referralTransaction: 20, earning: '$15,000', },
  { Sno: '5', username: 'Lawrence V. Davis', confidenceLevel: 50, referralTransaction: 20, earning: '$15,000', },
  { Sno: '6', username: 'Jason Stataham', confidenceLevel: 70, referralTransaction: 20, earning: '$15,000', },
  { Sno: '7', username: 'Paul Walker', confidenceLevel: 50, referralTransaction: 20, earning: '$12,000', },
  { Sno: '8', username: 'Van Diesel', confidenceLevel: 70, referralTransaction: 20, earning: '$15,000', },
  { Sno: '9', username: 'Anthony M. Iles', confidenceLevel: 30, referralTransaction: 20, earning: '$15,000', }
]

export default referralTransaction


