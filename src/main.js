import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store' 
import axios from 'axios'

import { BootstrapVue, BootstrapVueIcons, DropdownPlugin } from 'bootstrap-vue' 
import ToggleButton from 'vue-js-toggle-button'
import VTooltip from 'v-tooltip'
import Notifications from 'vue-notification'
import BackToTop from 'vue-backtotop'

import VueResource from "vue-resource" 
import CountryFlag from 'vue-country-flag'
import CoreuiVue from '@coreui/vue';
import OtpInput from "@bachdgvn/vue-otp-input";
import { iconsSet as icons } from './assets/icons/icons.js'
import VueClipboard from 'vue-clipboard2'
import VueSocialSharing from 'vue-social-sharing'

import './assets/custom.scss'
import './assets/common.scss'


Vue.use(require('vue-moment'));   
Vue.use(ToggleButton)
Vue.use(DropdownPlugin)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(VueResource);
Vue.use(VTooltip)
Vue.use(Notifications) 
Vue.use(BackToTop)
Vue.component('country-flag', CountryFlag)
Vue.use(CoreuiVue);
Vue.use(VueSocialSharing);

Vue.component("v-otp-input", OtpInput);
VueClipboard.config.autoSetContainer = true // add this line
Vue.use(VueClipboard)
Vue.http.options.emulateJSON = true;
axios.defaults.baseURL=process.env.VUE_APP_BASE_API_URL
axios.interceptors.response.use(
  response => response,
  error => {
      if (error.response.status == 401 && router.currentRoute.name != 'login') {
        
        
          localStorage.removeItem('user')
          router.push({ name: 'login' })
       
      }
      return Promise.reject(error)
  }
)

Vue.config.productionTip = false
export const bus = new Vue(); 

new Vue({
  router,
  store,
  icons,
  created () {
    let user = localStorage.getItem('user')
    if (user) {
      this.$store.commit('auth/SET_USER', JSON.parse(user))
    }
  },
  render: h => h(App)
}).$mount('#app');
