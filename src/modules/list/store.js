// import axios from 'axios'
// import Vue from 'vue'

const store = { namespaced: true }

// const listDefaults = () => ({ name: '', category_id: '', editMode: 0 })
// const cardDefaults = (position = 0) => ({ title: '', note: '', position, image:'' }) 

store.state = {
    lists: [],
    currentList: {},
    commentList: {},
    comments: [],
    categories: [],
}

store.mutations = {
//     SET_LISTS(state, lists) {
//         state.lists = lists.map(list => Object.assign(listDefaults(), list))
//     },
//     SET_CATEGORIES(state, categories) {
//         state.categories = categories.map(category => Object.assign(listDefaults(), category))
//     },

//     ADD_LIST(state) {
//         if (state.lists.length == 0 || state.lists[0].id) {
//             state.lists.unshift(listDefaults())
//         }
//     },
//     CREATE_LIST(state, list) {
//         state.lists.splice(0, 1, Object.assign(listDefaults(), list))
//     },
//     UPDATE_LIST(state, { oldList, newList }) {
//         let index = state.lists.indexOf(oldList)
//         state.lists.splice(index, 1, Object.assign(listDefaults(), newList))
//     },
//     REMOVE_LIST(state, list) {
//         let index = state.lists.indexOf(list)
//         state.lists.splice(index, 1)
//     },
//     SET_CURRENT_LIST(state, list) {
//         state.currentList = Object.assign({}, list)
//     },
//     SET_TOGGLE_OFF(state) {
//         state.currentList.is_share = false
//         state.currentList.share_socially = 0 
//         state.currentList.status = 0 
//     },
//     ADD_GROUP(state) {
//         let groups = state.currentList.groups
//         if (groups.length == 0) {
//             groups.push({
//                 list_id: state.currentList.id,
//                 title: '',
//                 position: 0
//             })
//             state.currentList.groups = groups
//         }
//         else if (groups[groups.length - 1].id) {   
//             groups.push({
//                 list_id: state.currentList.id,
//                 title: '',
//                 position: `${groups.length}`
//             })
//             state.currentList.groups = groups
//         }
//     },
//     CREATE_GROUP(state, group) {
//         state.currentList.groups.splice(state.currentList.groups.length-1, 1, group)
//     },
//     UPDATE_GROUP(state, { group, title }) {
//         group.title = title
//     },
//     REMOVE_GROUP(state, group) {
//         let index = state.currentList.groups.indexOf(group)
//         state.currentList.groups.splice(index, 1)
//     },
//     ADD_GROUP_CARD(state, group) {
//         if (!group.cards || group.cards.length == 0) {
//             Vue.set(group, 'cards', [cardDefaults()])
//         } else {
//             let lastCard = group.cards[group.cards.length - 1]
//             if (lastCard.id) {
//                 group.cards.push(cardDefaults(++lastCard.position))
//             }
//         }
//     },
//     CREATE_GROUP_CARD(state, { group, card }) {
//         let original = group.cards[group.cards.length - 1]
//         original.id = card.id
//     },
//     UPDATE_CARD_ORDER(state, { group, cards }) {
//         group.cards = cards.map((card, index) => {
//             return Object.assign(card, { position: index, group_id: group.id })
//         })
//     },
//     UPDATE_GROUP_ORDER(state, { group}) {
//         group = group.map((group, index) => {
//             return Object.assign(group, { position: index, id: group.id })
//         })
//         state.currentList.groups = group
//     },
//     REMOVE_GROUP_CARD(state, { group, card }) {
//         let index = group.cards.indexOf(card)
//         group.cards.splice(index, 1)
//     },
// }

// store.actions = {
//     loadLists({ commit }) {
//         axios.get('/lists')
//             .then(({ data }) => {
//                 commit('SET_LISTS', data.data)
//             })
//     },
//     loadCategories({ commit }) {
//         axios.get('/categories')
//             .then(({ data }) => {
//                 commit('SET_CATEGORIES', data.data)
//             })
//     },
    
//     addList({ commit }) {
//         commit('ADD_LIST')
//     },
//     createList({ commit }, list) {
//         axios.post('lists', list)
//             .then(({ data }) => {
//                 commit('CREATE_LIST', data.data)
//             })
//     },
//     updateList({ commit }, { list, name , category_id}) {    
//         axios.put(`lists/${list.id}`, { name , category_id }) 
//             .then(({ data }) => {
//                 commit('UPDATE_LIST', { oldList: list, newList: data.data })
//             })
//     },
//     deleteList({ commit }, list) {
//         axios.delete(`lists/${list.id}`)
//             .then(() => {
//                 commit('REMOVE_LIST', list)
//             })
//     },
//     loadList({ commit }, listId) {
//         axios.get(`lists/${listId}`)
//             .then(({ data }) => {
//                 commit('SET_CURRENT_LIST', data.data)
//             })
//     },
//     addGroup({ commit }) {
//         commit('ADD_GROUP')
//     },
//     createGroup({ commit, state }, group) {
//         axios.post(`lists/${state.currentList.id}/groups`, group)
//             .then(({ data }) => {
//                 commit('CREATE_GROUP', data.data)
//             })
//     },
//     updateGroup({ commit, state }, { group, title }) {
//         axios.put(`lists/${state.currentList.id}/groups/${group.id}`, { title })
//             .then(() => {
//                 commit('UPDATE_GROUP', { group, title })
//             })
//     },
//     deleteGroup({ state, commit }, group) {
//         if (group.id) {
//             axios.delete(`lists/${state.currentList.id}/groups/${group.id}`)
//                 .then(() => {
//                     commit('REMOVE_GROUP', group)
//                 })
//         } else {
//             commit('REMOVE_GROUP', group)
//         }
//     },
//     addGroupCard({ commit }, group) {
//         commit('ADD_GROUP_CARD', group)
//     },
//     createGroupCard({ state, commit }, { group, card }) {
//         axios.post(`lists/${state.currentList.id}/groups/${group.id}/cards`, card)
//             .then(({ data }) => {
//                 commit('CREATE_GROUP_CARD', { group, card: data.data })
                
//                 //focus error on card desciption
//                 // commit('SET_CURRENT_LIST', data.list) 
//                 commit('SET_TOGGLE_OFF') 
//             })
//     },
//     updateGroupCard({ state, commit }, { group, card }) {
//         axios.put(`lists/${state.currentList.id}/groups/${group.id}/cards/${card.id}`, card)
//         .then((data)=>{
//             data = data.data
//             commit('SET_CURRENT_LIST', data.list)
//         })
//     },
//     updateGroupOrder({ state, commit }, { group}) {  
//         commit('UPDATE_GROUP_ORDER', { group })
//         axios.post(`lists/${state.currentList.id}/groups/reorder`, {
//             groups: group
//         })
//         .then(({ data }) => {
//             commit('UPDATE_GROUP_ORDER', { group:data.data })
//             state.currentList.groups = data.data
//         })
//     },
//     deleteGroupCard({ state, commit }, { group, card }) {
//         if (card.id) {
//             axios.delete(`lists/${state.currentList.id}/groups/${group.id}/cards/${card.id}`)
//                 .then(() => {
//                     commit('REMOVE_GROUP_CARD', { group, card })
//                     commit('SET_TOGGLE_OFF')  
//                 })
//         } else {
//             commit('REMOVE_GROUP_CARD', { group, card })
//         }
//     },
//     updateCardOrder({ state, commit }, { group, cards }) {
//         commit('UPDATE_CARD_ORDER', { group, cards })
//         commit('SET_TOGGLE_OFF') 
//         axios.post(`lists/${state.currentList.id}/groups/${group.id}/cards/reorder`, {
//             cards: group.cards
//         })
//         .then(({ data }) => {
//             commit('UPDATE_CARD_ORDER', { group, cards: data.data })
//             commit('SET_TOGGLE_OFF') 
//         })
//     },
//     createShareKey({ getters }) {
//         return axios.post(`lists/${getters.currentList.id}/share`)
//     }
}

store.getters = {
    lists: state => state.lists,
    categories: state => state.categories,
    currentList: state => state.currentList
}

export default store
