import axios from 'axios'

const store = { namespaced: true }
const data1 = {
    username :'simrata'
}
store.state = { user: null } 

store.mutations = {
    SET_USER(state, user) {
        state.user = user
        localStorage.setItem('user', JSON.stringify(user))
        axios.defaults.headers.common['Authorization'] = `Bearer ${user.token}`
    },
    CLEAR_USER() {
        localStorage.removeItem('user')
        localStorage.removeItem('share_id')
        localStorage.removeItem('publicsharekey')
        localStorage.removeItem('duplicatelist')
        localStorage.removeItem('duplicatelist1') 
        location.href = '/'
    }
}

store.actions = {
    register({ commit }, credentials) {
        return axios
            .post('/auth/register', credentials)
            .then(({ data }) => {
                commit('SET_USER', data)
            })
            // remove this catch when api work
            .catch(({data})=>{
                commit('SET_USER', data1)
                console.log(data)
            })
    },
    login({ commit }, credentials) {
        return axios
            .post('/auth/login', credentials)
            .then(({ data }) => {
                commit('SET_USER', data)
            })
            // remove this catch when api work
            .catch(({data})=>{
                commit('SET_USER', data1)
                console.log(data)
            })
    },
    logout({ commit }) {
        commit('CLEAR_USER')
    }
}

store.getters = {
    user: ({ user }) => user ? user.user : null,
    loggedIn: state => !!state.user
}

export default store
